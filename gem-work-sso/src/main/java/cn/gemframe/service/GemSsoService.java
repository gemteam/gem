package cn.gemframe.service;

import cn.gemframe.common.bean.response.RefreshTokenResp;
import cn.gemframe.response.ResultData;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;

public interface GemSsoService {

	/**
	 * 删除token
	 * @param request
	 * @return
	 */
	String deleteToken(HttpServletRequest request);


	/***
	 * 刷新token
	 * @param refresh_token
	 * @return
	 */
	ResultData<RefreshTokenResp> refreshToken(String refresh_token);
}
