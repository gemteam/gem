package cn.gemframe.service;

import java.util.List;

public interface GemPermisService {
	
	List<String> permitsList();
}
