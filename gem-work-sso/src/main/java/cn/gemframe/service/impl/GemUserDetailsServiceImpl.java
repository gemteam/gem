/**
 * @Title:业务实现
 * @Description:用户登录
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service.impl;

import cn.gemframe.dao.GemClientUserMapper;
import cn.gemframe.dao.GemUserMapper;
import cn.gemframe.po.GemClientUser;
import cn.gemframe.po.GemUser;
import cn.gemframe.config.security.bean.GemUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;

/**
 * @Title:业务实现
 * @Description:用户登录
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Component
public class GemUserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private GemUserMapper userMapper;
	@Autowired
	private GemClientUserMapper clientUserMapper;

	/**
	 * @Description:获取用户登录信息
	 * @param username 用户名
	 * @author: nine
	 * @date 2018年11月6日
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		GemUserDetails userData = null;
		GemUser user = getUserByName(username);
		if(user != null){
			userData = new GemUserDetails(username,user.getPassword());
			userData.setUserId(String.valueOf(user.getId()));
			userData.setUsername(user.getUsername());
			return userData;
		}

		GemClientUser clientUser = getUserByAppID(username);
		if(clientUser != null){
			userData = new GemUserDetails(username,clientUser.getAppSecret());
			userData.setUsername(clientUser.getAppid());
			return userData;
		}
		return userData;
	}

	/**
	 * @Description:根据用户名去数据库查询用户
	 * @param username 用户名
	 * @author: nine
	 * @date 2018年11月6日
	 */
	public GemUser getUserByName(String username) {
		Example userExample = new Example(GemUser.class);
		Criteria createCriteria = userExample.createCriteria();
		createCriteria.andEqualTo("username", username);
		List<GemUser> listUser = userMapper.selectByExample(userExample);
		if(listUser != null && listUser.size() > 0) {
			return listUser.get(0);
		}else{
			createCriteria.andEqualTo("phone", username);
			listUser = userMapper.selectByExample(userExample);
			if(listUser != null && listUser.size() > 0) {
				return listUser.get(0);
			}else{
				createCriteria.andEqualTo("email", username);
				listUser = userMapper.selectByExample(userExample);
				if(listUser != null && listUser.size() > 0) {
					return listUser.get(0);
				}
			}
		}
		return null;
	}
	/**
	 * @Description:根据用户名去数据库查询用户
	 * @param appid 用户名
	 * @author: nine
	 * @date 2018年11月6日
	 */
	public GemClientUser getUserByAppID(String appid) {
		Example userExample = new Example(GemClientUser.class);
		Criteria createCriteria = userExample.createCriteria();
		createCriteria.andEqualTo("appid", appid);
		List<GemClientUser> listUser = clientUserMapper.selectByExample(userExample);
		if(listUser!=null && listUser.size()>0) {
			return listUser.get(0);
		}
		return null;
	}

}
