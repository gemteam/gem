/**
 * @Title:常量
 * @Description:权限常量
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.common.constant;

/**
 * @Title:常量
 * @Description:系统常量
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
public interface Constants {

	public interface AuthorizedGrantTypes{

		public static final String AUTHORIZATION_CODE = "authorization_code";//全部
		public static final String CLIENT_CREDENTIALS = "client_credentials";//全部
		public static final String REFESH_TOKEN = "refresh_token";//全部
		public static final String PASSWOED = "password";//全部
		public static final String IMPLICIT = "implicit";//全部

	}
}
