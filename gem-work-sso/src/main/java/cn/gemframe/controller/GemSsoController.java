package cn.gemframe.controller;

import cn.gemframe.common.bean.response.RefreshTokenResp;
import cn.gemframe.common.constant.RequestPath;
import cn.gemframe.constant.GemConstant;
import cn.gemframe.exception.GemExceptionHandler;
import cn.gemframe.exception.status.GemErrorStatus;
import cn.gemframe.service.GemSsoService;
import cn.gemframe.response.ResultData;
import cn.gemframe.utils.GemStringUtlis;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class GemSsoController extends GemExceptionHandler {

	@Autowired
	private GemSsoService gemTokenService;

	@RequestMapping(value = RequestPath.Auth.REFRESH_TOKEN,method = {RequestMethod.GET,RequestMethod.POST})
	public ResultData<RefreshTokenResp> refreshToken(String refresh_token) {
        log.error("---refresh_token:{}",refresh_token);
	    if(GemStringUtlis.isBlank(refresh_token)){
            return ResultData.getIllegalArgumentResult();
        }
		return gemTokenService.refreshToken(refresh_token);
	}

	@RequestMapping(value = RequestPath.Auth.ERROR,method = {RequestMethod.GET,RequestMethod.POST})
	public ResultData loginError(HttpServletRequest request) {
		return ResultData.getResultWithCode(GemErrorStatus.AUTHENTICATION_FAILED);
	}

	@RequestMapping(value = RequestPath.Auth.LOGOUT,method = {RequestMethod.GET,RequestMethod.POST})
	public ResultData logout(HttpServletRequest request) {
		String deleteToken = gemTokenService.deleteToken(request);
		if(deleteToken!=null && deleteToken.equalsIgnoreCase(GemConstant.Result.SUCCESS)) {
			return ResultData.SUCCESS(null);
		}else {
//			return ResultData.builder()
//					.code(GemErrorStatus.OK.getCode())
//					.msg(GemErrorStatus.OK.getMsg())
//					.data(null)
//					.build();
			return ResultData.getResultWithCode(GemErrorStatus.LOGOUT_ERROR);
		}
//		return ResultData.<RefreshTokenResp>builder().code(Integer.parseInt(code)).msg(message).data(null).build();
	}
}
