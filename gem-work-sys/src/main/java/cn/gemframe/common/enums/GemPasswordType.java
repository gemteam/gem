package cn.gemframe.common.enums;

public enum GemPasswordType {
    DEFAULT(0, "默认密码"),
    USERNAME(1, "密码同用户名"),
    IDCARDLAST8(2, "身份证后8位"),
    ;

    private String name;
    private int value;

    private GemPasswordType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getName(int value) {
        for (GemPasswordType obj : GemPasswordType.values()) {
            if (obj.getValue() == value) {
                return obj.name;
            }
        }
        return DEFAULT.name;
    }

    public static GemPasswordType valueOf(int value) {
        for (GemPasswordType obj : GemPasswordType.values()) {
            if (obj.getValue() == value) {
                return obj;
            }
        }
        return DEFAULT;
    }

    public static GemPasswordType valueOfIntString(Object obj) {
        if (obj == null)
            return DEFAULT;
        return valueOf(Integer.valueOf(obj.toString()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
