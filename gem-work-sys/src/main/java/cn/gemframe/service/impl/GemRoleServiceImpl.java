/**
 * @Title:业务实现
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service.impl;

import cn.gemframe.service.GemDemoService;
import cn.gemframe.service.GemRoleService;
import cn.gemframe.vo.GemUserVo;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Title:示例业务实现
 * @Description:示例业务实现
 * @author zhangysh
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Service
public class GemRoleServiceImpl implements GemRoleService {

	@Override
	public GemUserVo save(GemUserVo userVo) {
		return null;
	}

	@Override
	public Integer delete(Long id) {
		return null;
	}

	@Override
	public Integer delete(Long[] ids) {
		return null;
	}

	@Override
	public GemUserVo update(GemUserVo userVo) {
		return null;
	}

	@Override
	public GemUserVo info(Long id) {
		return null;
	}

	@Override
	public Integer count(GemUserVo userVo) {
		return null;
	}

	@Override
	public List<GemUserVo> list(Example example) {
		return null;
	}

	@Override
	public List<GemUserVo> pagelist(Example example, Integer pageNum, Integer pageSize) {
		return null;
	}
}
