/**
 * @Title:业务实现
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service.impl;

import cn.gemframe.dao.GemDemoMapper;
import cn.gemframe.po.GemDemo;
import cn.gemframe.service.GemDemoService;
import cn.gemframe.utils.GemJsonUtils;
import cn.gemframe.vo.GemDemoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.List;

/**
 * @Title:示例业务实现
 * @Description:示例业务实现
 * @author zhangysh
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class GemDemoServiceImpl implements GemDemoService {


    @Autowired
    private GemDemoMapper demoMapper;

    @Override
    public GemDemoVo save(@Valid @RequestBody GemDemoVo gemDemoVo) {
        return null;
    }

    @Override
    public Integer delete(Long id) {
        return null;
    }

    @Override
    public Integer delete(Long[] ids) {
        return null;
    }

    @Override
    public GemDemoVo update(@Valid @RequestBody GemDemoVo gemDemoVo) {
        return null;
    }

    @Override
    public GemDemoVo info(Long id) {
        GemDemo demo = demoMapper.selectByPrimaryKey(id);
        return GemJsonUtils.classToClass(demo, GemDemoVo.class);
    }

    @Override
    public Integer count(GemDemoVo gemDemoVo) {
        GemDemo demo = GemJsonUtils.classToClass(gemDemoVo, GemDemo.class);
        return demoMapper.selectCount(demo);
    }

    @Override
    public List<GemDemoVo> list(Example example) {
        return null;
    }

    @Override
    public List<GemDemoVo> pagelist(Example example, Integer pageNum, Integer pageSize) {
        return null;
    }
}
