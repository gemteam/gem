/**
 * @Title:业务实现
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service.impl;

import cn.gemframe.constant.GemConstant;
import cn.gemframe.dao.GemUserMapper;
import cn.gemframe.dao.GemUserRoleMapper;
import cn.gemframe.mapper.GemBeanMapper;
import cn.gemframe.po.GemUser;
import cn.gemframe.po.GemUserRole;
import cn.gemframe.service.GemUserRoleService;
import cn.gemframe.service.GemUserService;
import cn.gemframe.utils.GemIdUtlis;
import cn.gemframe.utils.GemJsonUtils;
import cn.gemframe.utils.GemPasswdUtil;
import cn.gemframe.utils.GemStringUtlis;
import cn.gemframe.vo.GemUserRoleVo;
import cn.gemframe.vo.GemUserVo;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

/**
 * @Title:角色业务实现
 * @Description:角色管理
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Service
public class GemUserRoleServiceImpl implements GemUserRoleService {
	@Override
	public void update(Long userId, Long[] roleIds) {

	}

	@Override
	public GemUserRoleVo save(GemUserRoleVo gemUserRoleVo) {
		return null;
	}

	@Override
	public Integer delete(Long id) {
		return null;
	}

	@Override
	public Integer delete(Long[] ids) {
		return null;
	}

	@Override
	public GemUserRoleVo update(GemUserRoleVo gemUserRoleVo) {
		return null;
	}

	@Override
	public GemUserRoleVo info(Long id) {
		return null;
	}

	@Override
	public Integer count(GemUserRoleVo gemUserRoleVo) {
		return null;
	}

	@Override
	public List<GemUserRoleVo> list(Example example) {
		return null;
	}

	@Override
	public List<GemUserRoleVo> pagelist(Example example, Integer pageNum, Integer pageSize) {
		return null;
	}
}
