/**
 * @Title:业务实现
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.gemframe.constant.GemConstant;
import cn.gemframe.mapper.GemBeanMapper;
import cn.gemframe.po.GemDictionary;
import cn.gemframe.vo.GemDictionaryVo;
import cn.gemframe.exception.GemException;
import cn.gemframe.exception.status.GemErrorStatus;
import cn.gemframe.utils.GemIdUtlis;
import cn.gemframe.utils.GemJsonUtils;
import cn.gemframe.utils.GemStringUtlis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gemframe.dao.GemDictionaryMapper;
import cn.gemframe.service.GemDictionaryService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * @Title:业务实现
 * @Description:数据字典管理
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Slf4j
@Service
public class GemDictionaryServiceImpl implements GemDictionaryService {

	@Autowired
	private GemDictionaryMapper dictionaryMapper;

	/**
	 * @Description:添加数据字典
	 * @param dictionaryVo 数据字典对象
	 * @author: nine
	 * @date 2018年11月5日
	 */
	@Override
	public GemDictionaryVo save(GemDictionaryVo dictionaryVo) {
		dictionaryVo.setCreateTime(new Date());
		dictionaryVo.setUpdateTime(new Date());
        GemDictionary dictionary = GemJsonUtils.classToClass(dictionaryVo, GemDictionary.class);
        Long id = GemIdUtlis.Id();
		dictionary.setId(id);
		Integer ret = dictionaryMapper.insert(dictionary);
		if(null != ret && ret == GemConstant.Number.ONE){
			return dictionaryVo;
		}else{
			log.error("====保存系统字典失败：{}",ret);
			return null;
		}
	}

	/**
	 * @Description:根据主键删除字典
	 * @param ids 主键集合
	 * @author: nine
	 * @date 2018年11月5日
	 */
	@Override
	public Integer delete(Long[] ids) {
		if(ids!=null && ids.length>0) {
			for (Long id : ids) {
				return dictionaryMapper.deleteByPrimaryKey(id);
			}
		}
		return null;
	}
	/**
	 * @Description:根据主键删除字典
	 * @param id 主键集合
	 * @author: nine
	 * @date 2018年11月5日
	 */
	@Override
	public Integer delete(Long id) {
		return dictionaryMapper.deleteByPrimaryKey(id);
	}

	/**
	 * @Description:修改数据字典
	 * @param vo 数据字典对象
	 * @author: nine
	 * @date 2018年11月5日
	 */
	@Override
	public GemDictionaryVo update(GemDictionaryVo vo) {
		vo.setUpdateTime(new Date());
        GemDictionary dictionary = GemJsonUtils.classToClass(vo, GemDictionary.class);
		Integer ret = dictionaryMapper.updateByPrimaryKeySelective(dictionary);
		if(null != ret && ret == GemConstant.Number.ONE){
			return vo;
		}else{
			log.error("====保存系统字典失败：{}",ret);
			return null;
		}
	}


	/***
	 * 根据ID获取字典详情
	 * @param id
	 * @return
	 */
	@Override
	public GemDictionaryVo info(Long id) {
		if(id == null) {
			throw new GemException(GemErrorStatus.PARAMETER_ERROR);
		}
		GemDictionary dictionary= dictionaryMapper.selectByPrimaryKey(id);
		return GemJsonUtils.classToClass(dictionary,GemDictionaryVo.class);
	}

	@Override
	public Integer count(GemDictionaryVo gemDictionaryVo) {
		return null;
	}

	@Override
	public List<GemDictionaryVo> list(Example example) {
		return null;
	}

	@Override
	public List<GemDictionaryVo> pagelist(Example example, Integer pageNum, Integer pageSize) {
		return null;
	}


	@Override
	public List<GemDictionary> findDictionChildrenById(Long id) {
		return null;
	}

	/**
	 * @Description:查询字典列表
	 * @param name 名称
	 * @param code 编码
	 * @author: nine
	 * @date 2018年11月13日
	 */
	@Override
	public List<GemDictionary> findDictionMenu(String name, String code) {
		Example example = new Example(GemDictionary.class);
		Criteria createCriteria = example.createCriteria();
		createCriteria.andEqualTo("parentId",-1);
		List<String> dicTypeList = new ArrayList<>();
		dicTypeList.add("1");
		dicTypeList.add("2");
		createCriteria.andIn("dicType",dicTypeList);
		if(GemStringUtlis.isNotBlank(name) && !name.equalsIgnoreCase("null") && name.length()>0) {
			createCriteria.andLike("name","%"+name+"%");
		}
		if(GemStringUtlis.isNotBlank(code) && !code.equalsIgnoreCase("null") && code.length()>0) {
			createCriteria.andLike("code","%"+code+"%");
		}
		example.setOrderByClause("dic_sort asc");
		List<GemDictionary> selectByExample = dictionaryMapper.selectByExample(example);
		return getDicChildrenList(selectByExample,name,code);
	}

	/**
	 * @Description:递归遍历所有的字典组
	 * @param list 字典数据集合
	 * @author: nine
	 * @date 2018年11月5日
	 */
	public List<GemDictionary> getDicChildrenList(List<GemDictionary> list,String name, String code) {
		if(list!=null && list.size()>0) {
			for (GemDictionary dictionary : list) {
				Example example = new Example(GemDictionary.class);
				Criteria createCriteria = example.createCriteria();
				createCriteria.andEqualTo("parentId",dictionary.getId());
				List<String> dicTypeList = new ArrayList<>();
				dicTypeList.add("1");
				dicTypeList.add("2");
				createCriteria.andIn("dicType",dicTypeList);
				if(GemStringUtlis.isNotBlank(name) && !name.equalsIgnoreCase("null") && name.length()>0) {
					createCriteria.andLike("name","%"+name+"%");
				}
				if(GemStringUtlis.isNotBlank(code) && !code.equalsIgnoreCase("null") && code.length()>0) {
					createCriteria.andLike("code","%"+code+"%");
				}
				example.setOrderByClause("dic_sort asc");
				List<GemDictionary> selectByExample = dictionaryMapper.selectByExample(example);
				getDicChildrenList(selectByExample,name,code);
			}
		}
		return list;
	}

}
