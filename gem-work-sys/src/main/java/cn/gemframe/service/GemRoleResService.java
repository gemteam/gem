/**
 * @Title:业务接口
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.service;

import cn.gemframe.anti.service.GemBaseService;
import cn.gemframe.vo.GemUserVo;

/**
 * @Title:示例业务接口
 * @Description:示例接口
 * @author zhangysh
 * @date 2019年3月25日 10:05:36
 * @version V1.0
 */
public interface GemRoleResService extends GemBaseService<GemUserVo> {
}
