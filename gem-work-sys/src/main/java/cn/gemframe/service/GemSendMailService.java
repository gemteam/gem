package cn.gemframe.service;

public interface GemSendMailService {

	/**
	 * @Description:发送邮件
	 * @param title 标题
	 * @param toUser 接收人
	 * @param context 内容
	 * @author: zxy
	 * @date 2018年12月11日
	 */
	void sendMailService(String title, String toUser, String context);
}
