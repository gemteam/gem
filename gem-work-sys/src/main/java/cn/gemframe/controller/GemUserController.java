/**
 * @Title:控制器
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.controller;

import cn.gemframe.common.constants.RequestPath;
import cn.gemframe.constant.GemConstant;
import cn.gemframe.exception.GemException;
import cn.gemframe.exception.GemExceptionHandler;
import cn.gemframe.exception.status.GemErrorStatus;
import cn.gemframe.po.GemUser;
import cn.gemframe.response.ResultData;
import cn.gemframe.service.GemUserService;
import cn.gemframe.utils.GemStringUtlis;
import cn.gemframe.vo.GemUserVo;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.List;


/**
 * @Title:用户控制器
 * @Description:用户控制器
 * @author zhangysh
 * @date 2019年3月25日 10:04:51
 * @version V1.0
 */
@Slf4j
@RestController
public class GemUserController extends GemExceptionHandler {

    @Autowired
    private GemUserService gemUserService;

    /**
     * @Description:增加
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.User.SAVE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData save(@Valid @RequestBody GemUserVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        vo = gemUserService.save(vo);
        if(null == vo){
            return ResultData.getDBOperError(GemConstant.DBOperType.INSERT);
        }
        return ResultData.SUCCESS(vo);
    }


    /**
     * @Description:删除
     * @param ids 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.User.DELETE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData delete(@RequestBody Long[] ids) {
        if(null == ids || ids.length == 0){
            log.error("---数据ID不能为空");
            return ResultData.getIllegalArgumentResult();
        }

        Integer result = gemUserService.delete(ids);
        if(null == result){
            return ResultData.getDBOperError(GemConstant.DBOperType.DELETE);
        }
        return ResultData.SUCCESS(null);
    }


    /**
     * @Description:删除
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.User.UPDATE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData update(@Valid @RequestBody GemUserVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        vo = gemUserService.update(vo);
        if(null == vo){
            return ResultData.getDBOperError(GemConstant.DBOperType.UPDATE);
        }
        return ResultData.SUCCESS(vo);
    }

    /**
     * @Description:查询用户列表
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.User.LIST,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData<List<GemUserVo>> list(@RequestBody GemUserVo vo) {
        Example example = new Example(GemUser.class);
        Example.Criteria createCriteria = example.createCriteria();
        example.setOrderByClause(vo.getOrder());
        createCriteria.andIsNotNull("id");
        String userName = vo.getUsername();
        if(GemStringUtlis.isNotBlank(userName)&& !userName.equalsIgnoreCase("null")&& userName.length()>0) {
            createCriteria.andLike("username","%"+userName+"%");
        }
        String name = vo.getName();
        if(GemStringUtlis.isNotBlank(name) && !name.equalsIgnoreCase("null") && name.length()>0) {
            createCriteria.andLike("name","%"+name+"%");
        }
        String phone = vo.getPhone();
        if(GemStringUtlis.isNotBlank(phone) && !phone.equalsIgnoreCase("null") && phone.length()>0) {
            createCriteria.andLike("phone", "%"+phone+"%");
        }
        String email = vo.getEmail();
        if(GemStringUtlis.isNotBlank(email) && !email.equalsIgnoreCase("null") && email.length()>0) {
            createCriteria.andLike("email","%"+email+"%");
        }

        List<GemUserVo> list = gemUserService.list(example);
        return ResultData.SUCCESS(list);
    }

    /**
     * @Description:查询用户分页列表
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.User.PAGE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData<List<GemUserVo>> pagelist(@RequestBody GemUserVo vo) {
        log.info("---vo:{}",vo);
        PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
        Example example = new Example(GemUser.class);
        Example.Criteria createCriteria = example.createCriteria();
        example.setOrderByClause(vo.getOrder());
        createCriteria.andIsNotNull("id");
        String userName = vo.getUsername();
        if(null != userName && GemStringUtlis.isNotBlank(userName)) {
            createCriteria.andLike("username","%"+userName+"%");
        }
        List<GemUserVo> list = gemUserService.pagelist(example,vo.getPageNum(),vo.getPageSize());
        return ResultData.SUCCESS(list);
    }

}
