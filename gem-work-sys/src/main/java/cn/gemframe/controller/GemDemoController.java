/**
 * @Title:控制器
 * @Description:数据字典管理
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.controller;

import cn.gemframe.common.constants.RequestPath;
import cn.gemframe.constant.GemConstant;
import cn.gemframe.exception.GemExceptionHandler;
import cn.gemframe.response.ResultData;
import cn.gemframe.service.GemDemoService;
import cn.gemframe.utils.GemStringUtlis;
import cn.gemframe.vo.GemDemoVo;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.List;


/**
 * @Title:示例控制器
 * @Description:示例控制器
 * @author zhangysh
 * @date 2019年3月25日 10:04:51
 * @version V1.0
 */
@Slf4j
@RestController
public class GemDemoController extends GemExceptionHandler {

    @Autowired
    private GemDemoService gemDemoService;

    /**
     * @Description:增加
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.Demo.SAVE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData save(@Valid @RequestBody GemDemoVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        vo = gemDemoService.save(vo);
        if(null == vo){
            return ResultData.getDBOperError(GemConstant.DBOperType.INSERT);
        }
        return ResultData.SUCCESS(vo);
    }


    /**
     * @Description:删除
     * @param ids 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.Demo.DELETE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData delete(@RequestBody Long[] ids) {
        Integer result = gemDemoService.delete(ids);
        if(null == result){
            return ResultData.getDBOperError(GemConstant.DBOperType.DELETE);
        }
        return ResultData.SUCCESS(null);
    }


    /**
     * @Description:删除
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.Demo.UPDATE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData update(@Valid @RequestBody GemDemoVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        vo = gemDemoService.update(vo);
        if(null == vo){
            return ResultData.getDBOperError(GemConstant.DBOperType.UPDATE);
        }
        return ResultData.SUCCESS(vo);
    }

    /**
     * @Description:查询用户列表
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.Demo.LIST,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData<List<GemDemoVo>> list(@Valid @RequestBody GemDemoVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        //组装查询条件模版
        Example example = new Example(GemDemoVo.class);
        Example.Criteria createCriteria = example.createCriteria();
        //设置排序
        example.setOrderByClause(vo.getOrder());
        //设置主键
        createCriteria.andIsNotNull("id");
        //设置查询字段
        String desp = vo.getDesp();
        if(GemStringUtlis.isNotBlank(desp)
                && !desp.equalsIgnoreCase("null")
                && desp.length()>0) {
            createCriteria.andLike("desp","%"+desp+"%");
        }


        List<GemDemoVo> list = gemDemoService.list(example);
        return ResultData.SUCCESS(list);
    }

    /**
     * @Description:查询用户分页列表
     * @param vo 对象
     * @author: nine
     * @date 2018年11月5日
     */
    @RequestMapping(value = RequestPath.Demo.PAGE,method = {RequestMethod.GET,RequestMethod.POST})
    public ResultData<List<GemDemoVo>> pagelist(@Valid @RequestBody GemDemoVo vo,Errors errors) {
        if (errors.hasErrors()) {
            return ResultData.getIllegalArgumentResult(errors.getFieldError().getDefaultMessage());
        }
        //设置分页
        PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
        //组装查询条件模版
        Example example = new Example(GemDemoVo.class);
        Example.Criteria createCriteria = example.createCriteria();
        //设置排序
        example.setOrderByClause(vo.getOrder());
        //设置主键
        createCriteria.andIsNotNull("id");
        //设置查询条件
        String desp = vo.getDesp();
        if(GemStringUtlis.isNotBlank(desp)
                && !desp.equalsIgnoreCase("null")
                && desp.length()>0) {
            createCriteria.andLike("desp","%"+desp+"%");
        }

        List<GemDemoVo> list = gemDemoService.pagelist(example,vo.getPageNum(),vo.getPageSize());
        return ResultData.SUCCESS(list);
    }

}
