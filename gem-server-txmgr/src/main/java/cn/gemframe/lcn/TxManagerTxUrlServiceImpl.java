package cn.gemframe.lcn;

import com.codingapi.tx.config.service.TxManagerTxUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class TxManagerTxUrlServiceImpl implements TxManagerTxUrlService {

	@Autowired
	private Environment env;

	@Override
	public String getTxUrl() {
		return env.getProperty("tm.manager.url");
	}
}
