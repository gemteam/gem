/**
 * @Title:业务对象
 * @Description:业务对象的基类
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体对象基类
 */
@Data
@MappedSuperclass
public class GemBaseVo implements Serializable {

	private static final long serialVersionUID = -653633462559232667L;
	private Long id;
	private Integer states;
	private Date createTime;
	private Date updateTime;
	private String desp;

	//分页查询公共字段
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private @Transient()String order;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private @Transient()Integer pageNum;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private @Transient()Integer pageSize;

}
