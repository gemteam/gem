/**
 * @Title:返回对象
 * @Description:权限信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.vo;

import cn.gemframe.po.GemResource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Title:返回对象
 * @Description:权限信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class GemResourceVo extends GemBaseVo {

	private static final long serialVersionUID = 7866863740908080321L;

	private Long parentId;
	private String name;
	private String path;
	private String icon;
	private String code;
	private Integer levels;
	private Integer sort;
	private Integer type;

	private @Transient()
	List<GemResource> childrens;
	private @Transient() boolean selected;
}
