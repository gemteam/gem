/**
 * @Title:返回对象
 * @Description:词典信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.vo;

import cn.gemframe.po.GemDictionary;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @Title:返回对象
 * @Description:词典信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class GemDictionaryVo extends GemBaseVo {

	private static final long serialVersionUID = -3823692402471076373L;

	private String name;
	private String key;
	private Integer sort;
	private String value_cn;
	private String value_en;
	private String value_code;
}
