/**
 * @Title:返回对象
 * @Description:用户信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.vo;

import cn.gemframe.annotation.ValidMoblie;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Title:返回对象
 * @Description:用户信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
public class GemUserVo extends GemBaseVo {

	private static final long serialVersionUID = 1619313748527046563L;

	@NotNull(message = "用户名不能为空")
	private String username;
	private Integer type;
	private String password;
	@NotEmpty(message = "姓名不能为空")
	@Length(max = 30, min = 2,message = "姓名长度限制2~30字符")
	private String name;
	@ValidMoblie
	private String phone;
	@Email(message = "邮箱格式错误")
	private String email;
//	@ValidIDCard
	private String idcard;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date lastLoginTime;
	private Long orgId;

	//密码生成类型 0 默认密码 1 等于用户名 2 证件后8位
	@NotNull(message = "密码类型不能为空")
	private @Transient()Integer passtype;
	//角色集合
	private @Transient() Long[] roleIds;

}
