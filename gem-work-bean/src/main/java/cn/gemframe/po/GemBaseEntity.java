/**
 * @Title:业务对象
 * @Description:业务对象的基类
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体对象基类
 */
@Data
@MappedSuperclass
public class GemBaseEntity implements Serializable {
	private static final long serialVersionUID = 8107925410233983481L;
	@Id
	private @Column(name="id",			columnDefinition = "bigint(20) COMMENT '主键'") 					Long id;
	private @Column(name="states",		columnDefinition = "tinyint(1) COMMENT '状态 0 禁用，1启用'") 	Integer states;
	private @Column(name="create_time",	columnDefinition = "datetime COMMENT '创建时间'") 				Date createTime;
	private @Column(name="update_time",	columnDefinition = "datetime COMMENT '修改时间'") 				Date updateTime;
	private @Column(name="desp",		columnDefinition = "varchar(200) COMMENT '描述'") 				String desp;

	//分页查询公共字段
	//排序字段
	@JsonIgnore
	private @Transient()String order;
	@JsonIgnore
	private @Transient()Integer pageNum;
	@JsonIgnore
	private @Transient()Integer pageSize;

}
