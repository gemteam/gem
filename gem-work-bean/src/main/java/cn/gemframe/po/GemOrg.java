/**
 * @Title:业务对象
 * @Description:组织信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Title:业务对象
 * @Description:组织信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@Entity
@Table(name="gem_org")
@EqualsAndHashCode(callSuper = true)
public class GemOrg extends GemBaseEntity {

	private static final long serialVersionUID = 7334637056333112946L;

	private @Column(name="parent_id",	columnDefinition = "bigint(20) COMMENT '父级'") 	  		Long parentId;
	private @Column(name="name",		columnDefinition = "varchar(20) COMMENT '机构名称'") 		String name;
	private @Column(name="sort",		columnDefinition = "int(11) COMMENT '排序号'") 		  	Integer sort;

	private @Transient() List<GemOrg> childrens;

	public GemOrg() {
	}
}
