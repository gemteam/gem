/**
 * @Title:业务对象
 * @Description:词典信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Title:业务对象
 * @Description:词典信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@Entity
@Table(name="gem_dict")
@EqualsAndHashCode(callSuper = true)
public class GemDictionary extends GemBaseEntity {

	private static final long serialVersionUID = 1L;

	//为使命名更规范优雅，@Column(name="\"name\"") 防止关键字冲突，jpa建表失败
	private @Column(name="name",		columnDefinition = "varchar(25) COMMENT '名字'") String name;
	private @Column(name="key",			columnDefinition = "varchar(25) COMMENT '字典key'") String key;
	private @Column(name="sort",		columnDefinition = "int(11) COMMENT '排序号'") 	  Integer sort;
	private @Column(name="value_cn",	columnDefinition = "varchar(25) COMMENT '中文值'") String value_cn;
	private @Column(name="value_en",	columnDefinition = "varchar(25) COMMENT '英文值'") String value_en;
	private @Column(name="value_code",	columnDefinition = "varchar(25) COMMENT '数字值'") String value_code;

}
