/**
 * @Title:业务对象
 * @Description:用户信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Title:业务对象
 * @Description:用户信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
@Entity
@Table(name="gem_user")
public class GemUser extends GemBaseEntity {

	private static final long serialVersionUID = -7064733480623981178L;

	private @Column(name="username",	columnDefinition = "varchar(255) COMMENT '用户名'")				String username;
	@JsonIgnore
	private @Column(name="password",	columnDefinition = "varchar(255) COMMENT '密码'") 				String password;
	private @Column(name="type",	columnDefinition = "varchar(11) COMMENT '1代表是超级管理员'") 			Integer type;
	private @Column(name="name",	columnDefinition = "varchar(255) COMMENT '真实姓名'") 				String name;
	private @Column(name="phone",		columnDefinition = "varchar(255) COMMENT '手机号'") 				String phone;
	private @Column(name="email",		columnDefinition = "varchar(255) COMMENT '邮箱'") 				String email;
	private @Column(name="idcard",		columnDefinition = "varchar(255) COMMENT '证件号'") 				String idcard;
	private @Column(name="last_login_time",	columnDefinition = "datetime COMMENT '最后登陆时间'") 		Date lastLoginTime;
	private @Column(name="org_id",	 columnDefinition = "bigint(20) COMMENT '机构ID'") Long orgId;
}
