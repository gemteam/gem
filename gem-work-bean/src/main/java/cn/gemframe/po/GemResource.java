/**
 * @Title:业务对象
 * @Description:权限信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Title:业务对象
 * @Description:权限信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@Entity
@Table(name="gem_resource")
@EqualsAndHashCode(callSuper = true)
public class GemResource extends GemBaseEntity {

	private static final long serialVersionUID = 2484476118528833219L;

	private @Column(name="parent_id",	columnDefinition = "bigint(20) COMMENT '权限的父级'") 	Long parentId;
	private @Column(name="name",		columnDefinition = "varchar(255) COMMENT '权限名字'") 	String name;
	private @Column(name="path",		columnDefinition = "varchar(255) COMMENT '资源路径'") 	String path;
	private @Column(name="icon",		columnDefinition = "varchar(255) COMMENT '图标'")		String icon;
	private @Column(name="code",		columnDefinition = "varchar(255) COMMENT '权限编码'")	String code;
	private @Column(name="level",		columnDefinition = "int(11) COMMENT '权限的级别'") 		Integer levels;
	private @Column(name="sort",		columnDefinition = "int(11) COMMENT '排序号'") 			Integer sort;
	private @Column(name="type",	columnDefinition = "int(11) COMMENT '是否是菜单0为菜单1按钮'") Integer type;

	private @Transient() List<GemResource> childrens;
	private @Transient() boolean selected;
}
