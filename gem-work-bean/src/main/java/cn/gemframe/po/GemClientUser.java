/**
 * @Title:业务对象
 * @Description:用户信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Title:业务对象
 * @Description:用户信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Data
@Entity
@Table(name="gem_client_user")
@EqualsAndHashCode(callSuper = true)
public class GemClientUser extends GemBaseEntity {

	private static final long serialVersionUID = 1L;

	private @Column(name="appid",	columnDefinition = "varchar(255) COMMENT 'AppID'")				String appid;
	@JsonIgnore
	private @Column(name="app_secret",	columnDefinition = "varchar(255) COMMENT 'AppSecret'") 		String appSecret;
	private @Column(name="org_id",	 columnDefinition = "bigint(20) COMMENT '机构ID'") Long orgId;
}
