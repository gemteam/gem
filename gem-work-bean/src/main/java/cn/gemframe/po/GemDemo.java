/**
 * @Title:业务对象
 * @Description:用户信息
 * Copyright 2018 GemFrame技术团队 http://www.gemframe.cn
 * Company: DianShiKongJian (Beijing) Technology Co., Ltd.
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package cn.gemframe.po;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Title:示例业务对象
 * @Description:示例信息
 * @author nine
 * @date 2018-11-1 16:06:06
 * @version V1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
@Entity
@Table(name="gem_demo")
public class GemDemo extends GemBaseEntity {

	private static final long serialVersionUID = -2400931631961373192L;

	@Column(name="test",columnDefinition = "varchar(255) COMMENT 'test'")
	private String test;
}
