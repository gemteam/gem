/**
 * Copyright (c) 2019 gemframe.cn All rights reserved.
 *
 * http://www.gemframe.cn
 *
 * 版权所有，侵权必究！
 */

package cn.gemframe.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理器
 *
 * @author nine
 */
@RestControllerAdvice
public class RRExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(RRException.class)
	public ResultEntity handleRRException(RRException e){
		ResultEntity r = new ResultEntity();
		r.put("code", e.getCode());
		r.put("msg", e.getMessage());

		return r;
	}

	@ExceptionHandler(Exception.class)
	public ResultEntity handleException(Exception e){
		logger.error(e.getMessage(), e);
		return ResultEntity.error();
	}
}
