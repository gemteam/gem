/**
 * Copyright (c) 2019 gemframe.cn All rights reserved.
 *
 * http://www.gemframe.cn
 *
 * 版权所有，侵权必究！
 */

package cn.gemframe.dao;

import java.util.List;
import java.util.Map;

/**
 * 数据库接口
 *
 * @author nine
 */
public interface GeneratorDao {

    List<Map<String, Object>> queryList(Map<String, Object> map);

    Map<String, String> queryTable(String tableName);

    List<Map<String, String>> queryColumns(String tableName);
}
