/**
 * Copyright (c) 2019 gemframe.cn All rights reserved.
 *
 * http://www.gemframe.cn
 *
 * 版权所有，侵权必究！
 */

package cn.gemframe.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 *
 * @author nine
 */
@Mapper
public interface PostgreSQLGeneratorDao extends GeneratorDao {

}
