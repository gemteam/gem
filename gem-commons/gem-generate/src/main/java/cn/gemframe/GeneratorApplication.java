/**
 * Copyright (c) 2019 gemframe.cn All rights reserved.
 *
 * http://www.gemframe.cn
 *
 * 版权所有，侵权必究！
 */

package cn.gemframe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratorApplication.class, args);
	}
}
